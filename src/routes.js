import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import HomePage from './components/HomePage';
import LearnerAdmin from './components/LearnerAdmin';
import MarkerAdmin from './components/MarkerAdmin';
import ResultAdminPage from './components/ResultAdminPage';
import TestPage from './components/TestPage';
import ResultTest from './components/ResultTest';
import NotFoundPage from './components/NotFoundPage.js';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="learner-admin" component={LearnerAdmin}/>
    <Route path="marker-admin" component={MarkerAdmin}/>
    <Route path="result-admin" component={ResultAdminPage}/>
    <Route path="test" component={TestPage}/>
    <Route path="result-test" component={ResultTest}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);

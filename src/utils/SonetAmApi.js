import CryptoJS from 'crypto-js';

class SoNetAPI {

  constructor( publicKey, privateKey) {
    this.publicKey = publicKey;
    this.privateKey = privateKey;
  }

  createSignature(type, url, timestamp, data, privateKey) {
    //let buffer = new Buffer(CryptoJS.HmacSHA256(type + url + timestamp + data, privateKey));
    //return buffer.toString('base64');
    let cryptoArray = CryptoJS.HmacSHA256(type + url + timestamp + data, privateKey);
    let result = CryptoJS.enc.Base64.stringify(cryptoArray);
    return result;
  }

  createTimestamp() {
    return new Date().toISOString().substr(0,19) + 'Z';
  }

  getLearners() {
    console.log("getLearners called..." + this.publicKey + ' * ' + this.privateKey);
    console.log("date = " + this.createTimestamp() );
    console.log("sigcub = " + this.createSignature("1","2",this.createTimestamp(),"4",this.privateKey));
  }

}

export default SoNetAPI;

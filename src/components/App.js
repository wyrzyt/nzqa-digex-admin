import '../styles/app.css';

import React, { PropTypes } from 'react';
import Header from './Header';
import Footer from './Footer';


const App = (props) => {
  return (
    <div>
      <Header className="header"/>
      {props.children}
      <Footer/>
    </div>
  );
};

App.propTypes = {
  children: PropTypes.element
};

export default App;

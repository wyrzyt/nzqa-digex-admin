"use strict";

import React from 'react';
import {AgGridReact} from 'ag-grid-react';

import '../../node_modules/ag-grid/dist/styles/ag-grid.css';
import '../../node_modules/ag-grid/dist/styles/theme-fresh.css';
//import 'ag-grid-root/dist/styles/ag-grid.css';    //generates warning, but works...
//import 'ag-grid-root/dist/styles/theme-fresh.css';

class ResultGrid extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="ag-fresh">
        <AgGridReact
          // listen for events with React callbacks
//          onRowSelected={this.onRowSelected.bind(this)}
//          onCellClicked={this.onCellClicked.bind(this)}

          // binding to properties within React State or Props
//          showToolPanel={this.state.showToolPanel}
//          quickFilterText={this.state.quickFilterText}
//          icons={this.state.icons}

          // column definitions and row data are immutable, the grid
          // will update when these lists change
          columnDefs={[
            {headerName: "Learner", field: "learner_nsn", width: 86},
            {headerName: "Standard", field: "std_number", width: 86},
            {headerName: "Provider", field: "moe_provider_id", width: 86},
            {headerName: "Result", field: "result_code", width: 86},
            {headerName: "Marker", field: "marker_code", width: 86},
            {headerName: "Status", field: "status", width: 86}
          ]}

          rowData={this.props.rowData}

          // or provide props the old way with no binding
          rowSelection="multiple"
          enableSorting="true"
          enableFilter="true"
          rowHeight="22"
        />
      </div>
    );
  }
}

ResultGrid.propTypes = {
  rowData: React.PropTypes.array
};

ResultGrid.defaultProps = {
  rowData: []
};

export default ResultGrid;

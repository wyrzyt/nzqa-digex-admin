
import React from 'react';
import {Link} from 'react-router';
import { Panel } from 'react-bootstrap';

const HomePage = () => {

  return (
    <Panel>
      <div>
        <h2>Digital Exams Pilot 2016</h2>
        <h3>Integration Center</h3> <br/>
        <dl>
          <dt><Link to="/learner-admin">Learner Admin</Link></dt>
          <dd> - Enrol Learners in a Digital Exam</dd><br/>
          <dt><Link to="/marker-admin">Marker Admin</Link></dt>
          <dd> - Manage Marker Accounts across SoNet and Lastpass</dd><br/>
          <dt><Link to="/result-admin">Result Admin</Link></dt>
          <dd> - Monitor Digital Results from SoNet</dd><br/>
        </dl>
      </div>
    </Panel>
  );
};

export default HomePage;

import '../styles/app.css';

import React from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import {Link } from 'react-router';
import NavLink from './NavLink';

const Header = () => {

  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/"><b>DigEx Pilot 2016</b></Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse className="navcollapse">
        <Nav>
          <NavItem><NavLink eventKey={2} to="/learner-admin">Learner Admin</NavLink></NavItem>
          <NavItem><NavLink eventKey={1} to="/marker-admin">Marker Admin</NavLink></NavItem>
          <NavItem><NavLink eventKey={2} to="/result-admin">Result Admin</NavLink></NavItem>
          {/*
          <NavDropdown eventKey={3} title="CSV" id="csv-nav-dropdown">
            <MenuItem eventKey={3.1}>Import ...</MenuItem>
            <MenuItem eventKey={3.2}>Export</MenuItem>
            <MenuItem divider />
            <MenuItem eventKey={3.3}>Reset</MenuItem>
          </NavDropdown>
          */}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;

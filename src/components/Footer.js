import '../styles/app.css';

import React from 'react';
import {Navbar} from 'react-bootstrap';
//import {Nav, Navbar, NavItem} from 'react-bootstrap';
import {Link } from 'react-router';
//import NavLink from './NavLink';

const Footer = () => {

  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/test"><b>Toolkit:</b></Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      {/*
      <Navbar.Collapse className="navcollapse">
        <Nav>
          <NavItem><NavLink eventKey={2} to="/result-test">Result Test</NavLink></NavItem>
        </Nav>
      </Navbar.Collapse>
      */}
    </Navbar>

  );
};

export default Footer;

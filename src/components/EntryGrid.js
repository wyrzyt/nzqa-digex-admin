"use strict";

import React from 'react';
import {AgGridReact} from 'ag-grid-react';

import '../../node_modules/ag-grid/dist/styles/ag-grid.css';
import '../../node_modules/ag-grid/dist/styles/theme-fresh.css';
//import 'ag-grid-root/dist/styles/ag-grid.css';    //generates warning, but works...
//import 'ag-grid-root/dist/styles/theme-fresh.css';

class EntryGrid extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="ag-fresh">
        <AgGridReact
          columnDefs={[
            {headerName: "Learner", field: "learner_nsn", width: 84},
            {headerName: "Standard", field: "std_number", width: 84},
            {headerName: "Provider", field: "moe_provider_id", width: 84},
            {headerName: "Result", field: "result", width: 84},
            {headerName: "Marker", field: "marker", width: 84},
            {headerName: "Status", field: "status", width: 84}
          ]}
          rowData={this.props.rowData}
          rowSelection="multiple"
          enableSorting="true"
          enableFilter="true"
          rowHeight="22"
        />
      </div>
    );
  }
}

EntryGrid.propTypes = {
  rowData: React.PropTypes.array
};

EntryGrid.defaultProps = {
  rowData: []
};

export default EntryGrid;


import React from 'react';
import {Link} from 'react-router';
import { Panel } from 'react-bootstrap';

const TestPage = () => {

  return (
    <Panel>
      <div>
        <h2>Digital Exams Pilot 2016</h2>
        <h3>Test Toolkit</h3> <br/>
        <dl>
          <dt><Link to="/result-test">Result Test</Link></dt>
          <dd> - Simulate results submission from SoNet</dd><br/>
        </dl>
      </div>
    </Panel>
  );
};

export default TestPage;

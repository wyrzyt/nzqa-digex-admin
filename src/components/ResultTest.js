import React from 'react';
import { Panel, Button } from 'react-bootstrap';
import Swagger from 'swagger-client';
import { Table } from 'reactable';


class ResultTest extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      tableData: []
    };

    this.addResult = this.addResult.bind(this);
  }

  addResult() {
    console.log('addResult.. calling server');

    let client = new Swagger({
      url: 'http://localhost:8888/api-docs',
      success: function() {
        client.internal.getResults({},{responseContentType: 'application/json'},function(result){
          console.log('results', result);
          this.setState({ tableData: result.obj});  // eslint-disable-line react/no-set-state
        }.bind(this),
          function(error) {
            console.log('failed with the following: ' + error.statusText);
          }.bind(this));
      }.bind(this)
    });


  }

  render() {

    const header = (
      <div>
        <Button onClick={this.addResult}>Add Result</Button>
      </div>
    );

    const footer = (
      <div>

      </div>
    );

    return (
      <Panel header={header} footer={footer}>
        <div>
          <h2 className="alt-header">Result Test</h2>
          <Table
            className="table"
            id="table"
            columns={[
              {label:"Learner", key:"learner_nsn"},
              {label:"Provider", key:"moe_provider_id"},
              {label:"Marker", key:"moe_provider_id"},
              {label:"Standard", key:"std_number"},
              {label:"Result", key:"result"}
            ]}
            data={this.state.tableData}
            noDataText="No matching records found."
            itemsPerPage={5}
            pageButtonLimit={5}
            currentPage={0}
            nextPageLabel=" NEXT"
            previousPageLabel="PREV "
            sortable={true}
            filterable={['learner_nsn']}
          />
        </div>
      </Panel>
    );
  }
}

export default ResultTest;

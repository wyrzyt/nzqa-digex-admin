// modules/NavLink.js
import React from 'react';
import { Link } from 'react-router';

// export default React.createClass({
//   render() {
//     return <Link {...this.props} activeClassName="active"/>
//   }
// })

class NavLink extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Link {...this.props} activeClassName="active"/>
    );
  }

}

// const NavLink = () => {
//   return (
//     <Link {...this.props} activeClassName="active"/>
//   );
// };

export default NavLink;

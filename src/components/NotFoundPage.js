import React from 'react';
import { Link } from 'react-router';
import { Panel } from 'react-bootstrap';

const NotFoundPage = () => {
  return (
    <Panel>
      <div>
        <h3>Page Not Found</h3>
        <Link to="/"> Go back to homepage </Link>
      </div>
    </Panel>
  );
};

export default NotFoundPage;

import React from 'react';
import { Panel, Button } from 'react-bootstrap';
import {AgGridReact} from 'ag-grid-react';
import Swagger from 'swagger-client';
import Papa from 'papaparse';
import ReactFileDownload from 'react-file-download';

class LearnerAdmin extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      resultData: []
    };
  }

  componentDidMount() {
    this.loadResults();
  }

  importCSV(event) {
    console.log("-----> importCSV: ", JSON.stringify(event.target.files[0]));
    console.log("ffiles", JSON.stringify(event.target.value,0,2));
    let file = event.target.files[0];
    Papa.parse(file, {
      complete: function(results) {
        console.log("Papa Finished:", results.data);
      }
    });
  }

  exportCSV() {
    ReactFileDownload(Papa.unparse(this.state.resultData), 'digex_results.csv');
  }

  loadResults() {
    console.log('loadResults.. calling server');

    let client = new Swagger({
      url: 'http://localhost:8888/api-docs',
      success: function() {
        client.internal.getResults({},{responseContentType: 'application/json'},function(result){
            console.log('results', result);
            this.setState({ resultData: result.obj});  // eslint-disable-line react/no-set-state
          }.bind(this),
          function(error) {
            console.log('failed with the following: ' + error.statusText);
          }.bind(this));
      }.bind(this)
    });
  }
//          <Button><label htmlFor="myfile">Import CSV</label></Button>
  render() {

    const header = (
      <div className="gocontainer">
        <input className="hidden" type="file" id="myfile" name="myfile" title="Load File" onChange={this.importCSV.bind(this)}/>
        <div className="goleft">
          <h4 className="alt-header">Learner Entries</h4>
        </div>
        <div className="goright">
          <Button onClick={this.loadResults.bind(this)}>Refresh</Button>
          <div className="godivider"/>
          <label className="btn btn-default" htmlFor="myfile">Import CSV</label>
          <Button onClick={this.exportCSV.bind(this)}>Export CSV</Button>
        </div>
      </div>
    );

    const footer = (
      <div className="gocontainer">
        <div className="goleft">
        </div>
        <div className="goright">
          <Button disabled>Sync NZQA</Button>
        </div>
      </div>
    );

    return (
      <Panel header={header} footer={footer}>
        <div>
          <div className="ag-fresh">
            <AgGridReact
              columnDefs={[
                {headerName: "Learner", field: "learner_nsn", width: 125},
                {headerName: "Standard", field: "std_number", width: 125},
                {headerName: "Provider", field: "moe_provider_id", width: 125},
                {headerName: "Entry", field: "entry_id", width: 125}
              ]}
              rowData={this.state.resultData}
              rowSelection="multiple"
              enableSorting="true"
              enableFilter="true"
              rowHeight="22"
            />
          </div>
        </div>
      </Panel>
    );
  }
}

export default LearnerAdmin;

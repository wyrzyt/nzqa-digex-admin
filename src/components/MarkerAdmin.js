import React from 'react';
import { Panel, Button } from 'react-bootstrap';

class MarkerAdmin extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  importCSV() {
    console.log('Import CSV Called');
  }

  exportCSV() {
    console.log('Export CSV Called');
  }

  syncLastpass() {
    console.log('Sync Lastpass');
  }

  syncSoNet() {
    console.log('Sync SoNet');
  }

  syncNZQA() {
    console.log('Sync NZQA');
  }

  render() {

    const header = (
      <div className="gocontainer">
        <div className="goleft">
          <h4 className="alt-header">Marker Provisioning</h4>
        </div>
        <div className="goright">
          <div className="godivider"/>
          <Button onClick={this.importCSV}>Import CSV</Button>
          <Button onClick={this.exportCSV} disabled="true">Export CSV</Button>
        </div>
      </div>
    );

    const footer = (
      <div className="gocontainer">
        <div className="goright">
          <Button  onClick={this.syncLastpass}>Sync Lastpass</Button>
          <div className="godivider"/>
          <Button onClick={this.syncSoNet}>Sync SoNet</Button>
          <div className="godivider"/>
          <Button onClick={this.syncNZQA}>Sync NZQA</Button>
        </div>
      </div>
    );

    return (
      <Panel header={header} footer={footer}>
        <div>
          <h2 className="alt-header">Marker Admin</h2>
            <p>TBD</p>
        </div>
      </Panel>
    );
  }
}

export default MarkerAdmin;

import React from 'react';
import { Panel, Button } from 'react-bootstrap';
import Swagger from 'swagger-client';
import ResultGrid from './ResultGrid';
import Papa from 'papaparse';
import ReactFileDownload from 'react-file-download';
import SonetAmApi from '../utils/SonetAmApi';

class ResultAdmin extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      hideGrid: false,
      resultData: []
    };
  }

  componentDidMount() {
    this.loadResults();
  }

  toggleGrid() {
    this.setState( { hideGrid: !this.state.hideGrid });  // eslint-disable-line react/no-set-state
  }

  exportCSV() {
    ReactFileDownload(Papa.unparse(this.state.resultData), 'digex_results.csv');
  }

  loadResults() {
    console.log('loadResults.. calling server');

    let client = new Swagger({
      url: 'http://localhost:8888/api-docs',
      success: function() {
        client.internal.getResults({},{responseContentType: 'application/json'},function(result){
            console.log('results', result);
            this.setState({ resultData: result.obj});  // eslint-disable-line react/no-set-state
          }.bind(this),
          function(error) {
            console.log('failed with the following: ' + error.statusText);
          }.bind(this));
      }.bind(this)
    });
  }

  loadSoNetResults() {
    let api = new SonetAmApi( 'x', 'y');
    api.getLearners();
  }

  render() {

    const header = (
      <div className="gocontainer">
        <div className="goleft">
          <h4 className="alt-header">Result Admin</h4>
        </div>
        <div className="goright">
          <Button onClick={this.loadSoNetResults.bind(this)}>Import from SoNet?</Button>
          <div className="godivider"/>
          <div className="godivider"/>
          <div className="godivider"/>
          <Button onClick={this.loadResults.bind(this)}>Refresh</Button>
          <div className="godivider"/>
          <Button onClick={this.exportCSV.bind(this)}>Export CSV</Button>
        </div>
      </div>
    );

    const footer = (
      <div className="gocontainer">
        <div className="goleft">
        </div>
        <div className="goright">
          <Button className={this.state.hideGrid ? 'hidden' : ''} onClick={this.toggleGrid.bind(this)} disabled>Timed Resubmit...</Button>
          <Button onClick={this.toggleGrid.bind(this)} disabled>Resubmit</Button>
        </div>
      </div>
    );

    return (
      <Panel header={header} footer={footer}>
        <div className={this.state.hideGrid ? 'hidden' : ''} >
          <ResultGrid rowData={this.state.resultData}/>
        </div>
      </Panel>
    );
  }
}

export default ResultAdmin;
